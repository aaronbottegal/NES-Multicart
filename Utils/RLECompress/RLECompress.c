//Version 1.0 Update 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void OutputCharacter(void);

FILE *InputFile;
FILE *OutputFile;

int Loop;

char OutputFileName[512];
char CompressFlag;

unsigned int FileLoop;
unsigned long int InputFileSize;
unsigned long int OutputFileSize;
unsigned long int CharacterSameCount;

unsigned long int DecideInstancesArray[256];
unsigned long int DecideLowestCase;
int DecideRead;

int LastCharacter;
int CurrentCharacter;

int has_progress;

int main(int argc, char *argv[]){
if (argc < 2){printf("\nNo file pointers! Aborting.\n\n");return 0;} //You're doing it wrong.

has_progress=1; //Use for later loops that need to run until I "break" them.
for (FileLoop=1;FileLoop<argc;FileLoop++){ //Loop the number of file pointers.

    InputFile=fopen(argv[FileLoop],"rb"); //Open file.
    if (InputFile==NULL){printf("\n*FAILED TO OPEN FILE %s*\n",argv[FileLoop]);continue;} //Failed to open, move on.

    InputFileSize=0;OutputFileSize=0; //Set variables.
    for (Loop=0;Loop<=255;Loop++){DecideInstancesArray[Loop]=0;} //Clear the array DecideInstancesArray for deciding which byte to use with the compression.
    while (has_progress==1){
        DecideRead=fgetc(InputFile); //Get byte
        if (DecideRead==EOF){break;} else {DecideInstancesArray[DecideRead]++;InputFileSize++;} //If not EOF, increment the array instances byte.
        }

    DecideLowestCase=DecideInstancesArray[0];CompressFlag=0; //Set the variables to *something*.

    for(Loop=0;Loop<=255;Loop++){ //Loop through all instance checks.
        if (DecideInstancesArray[Loop]==0){DecideLowestCase=0;CompressFlag=Loop;break;} //If zero, use that as flag since it was never used. This byte will be the first byte of the file.
        if (DecideInstancesArray[Loop]<DecideLowestCase){DecideLowestCase=DecideInstancesArray[Loop];CompressFlag=Loop;} //New lower case, remember it.
        }

    rewind(InputFile); //Set input file back to beginning.
    strcpy(OutputFileName,"RLECompressed"); //Name that comes before the file now.
    strncat(OutputFileName,argv[FileLoop],256); //256 character file name limit.
    OutputFile=fopen(OutputFileName,"wb"); //Open the output file.
    if (OutputFile!=NULL){printf("\nCompressing file: %s to %s",argv[FileLoop],OutputFileName);}
    else
    {printf("\n*FAILED TO OPEN OUTPUT FILE %s*",OutputFileName);fclose(InputFile);continue;} //Failed, close input file and move on to next file.
    LastCharacter=fgetc(InputFile); //Get first character.
    if (LastCharacter!=EOF){fputc(CompressFlag,OutputFile);OutputFileSize++;} //Not EOF, input file bigger than 0 bytes, put compress byte.
    while(LastCharacter!=EOF){ //Loop if there wasn't an EOF off the bat.

        CurrentCharacter=fgetc(InputFile); //Get next character.

        if (CurrentCharacter!=LastCharacter){ //Not the same, output last character.
            CharacterSameCount=1; //Tell output compressor not long enough and how many.
            OutputCharacter(); //Output.
            LastCharacter=CurrentCharacter; //Move next character to current buffer.
            continue; //Process nxt character.
        }

        if (CurrentCharacter==LastCharacter){
            CharacterSameCount=1; //Character count-1.
            while(CurrentCharacter==LastCharacter){ //While rgabbing the same byte, loop.
                CurrentCharacter=fgetc(InputFile); //Get new current.
                CharacterSameCount++; //Update input length.
                }
            OutputCharacter(); //Output data.
            if (CurrentCharacter==EOF){break;} //New character is EOF, end. Don't loop again as no new data.
            LastCharacter=CurrentCharacter; //New data,put next character to last buffer.
            continue;
        }
    }
    fputc(CompressFlag,OutputFile);fputc(CompressFlag,OutputFile);fputc(0,OutputFile);OutputFileSize=OutputFileSize+1; //"EOF" marker.
    printf("\nOriginal Size:\t0x%lx\nNew Size:\t0x%lx\nCompress Byte:\t0x%x, used %lu times.\n",InputFileSize,OutputFileSize,CompressFlag,DecideLowestCase); //Display info.
    fclose(OutputFile); //Close files.
    fclose(InputFile);
}
printf("\nCompression complete.\n"); //Done with whole program.
return 0; //Return.
}

void OutputCharacter(void){
while (CharacterSameCount>255){ //Greater than 1 byte big repeat value;
    OutputFileSize=OutputFileSize+3; //Output file is about to get 3 bytes bigger.
    fputc(CompressFlag,OutputFile); //Put compress flag.
    fputc(LastCharacter,OutputFile); //Put byte outputting.
    fputc(255,OutputFile); //Put repeat number.
    CharacterSameCount=CharacterSameCount-255; //Subtract characters left.
    }

if (CharacterSameCount>2 || LastCharacter==CompressFlag){ //Either writing character=flag or more then 2 characters in a row.
    OutputFileSize=OutputFileSize+3; //Going to get 3 bigger.
    fputc(CompressFlag,OutputFile); //Put compress flag.
    fputc(LastCharacter,OutputFile); //Put compressing character.
    fputc(CharacterSameCount,OutputFile); //Put count.
    CharacterSameCount=0; //No more characters to put up now.
    }

while (CharacterSameCount>0){ //If true, will be some extras that need to be put to file that get no special treatment at all.
    fputc(LastCharacter,OutputFile);
    OutputFileSize++;
    CharacterSameCount--; //Put to file, increment size and characters left to output.
    }
}
