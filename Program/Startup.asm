Reset:
  SEI ;Disable IRQs
  CLD ;Disable decimal mode
  LDX #$40
  STX $4017 ;Disable APU frame IRQ
  LDX #$FF
  TXS ;Set up stack
  INX ;X=0
  STX $2000 ;Disable NMI
  STX $2001 ;Disable Rendering
  STX $4010 ;Disable DMC IRQs
  BIT $2002 ;Reset NMI flag if set.
.StartupVBlankWait1:
  BIT $2002 ;Test NMI flag.
  BPL .StartupVBlankWait1 ;If NMI hasn't occured, loop.
  LDA #$07 ;NMI happened, now we clear RAM 0x07FF to 0x0000 with this program.
  STA <ZeroWrite ;Set high byte of Zero zeropage pointer.
  LDY #$00
  STY <Zero ;Reset low byte just in case it was trashed by a CPU reset.
  TYA ;Y=0, A=Y so A=0.
.ClearRAM:
  DEY ;Y=Y-1
  STA [Zero],Y ;Store at Zero pointer+Y.
  BNE .ClearRAM ;If the DEY resulted in anything but a 0, keep looping.
  DEC <Zero+1 ;Decrement the high byte of the page pointer.
  BMI .StartupVBlankWait2 ;If it goes under 0, exit, all RAM has been cleared.
  LDX <ZeroWrite ;Load value.
  CPX #$02 ;If equal to the 0x02XX page, Sprites. Sprites need to be cleared to FF so they are off the screen.
  BNE .ClearRAM-1 ;Nope, put Y to A. (Makes a 00)
  LDA #$FF ;Yes we're on sprite page, load A with FF to clear sprites!
  BNE .ClearRAM ;Loop again. Since the LDA #$FF made this always true, we use BNE instead of JMP to save 1 byte of ROM.
.StartupVBlankWait2:      ;Second wait for vblank, PPU is ready for writes after this happens.
  BIT $2002
  BPL .StartupVBlankWait2