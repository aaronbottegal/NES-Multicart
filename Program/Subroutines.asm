DecompressToPPU:
  LDA RleCompressPointersLow,X
  STA <DecompressDataPointer
  LDA RleCompressPointersHigh,X
  STA <DecompressDataPointer+1
DecompressToPPUPreset:
  LDY #$00
  LDA [DecompressDataPointer],Y
  STA <CompressedDataFlag
  INY
.DecompressMainLoop:
  LDA [DecompressDataPointer],Y
  CMP <CompressedDataFlag
  BEQ .CompressedData
  STA $2007
.DecompressMainLoopReEnter:
  INY  
  BNE .DecompressMainLoop
  INC <DecompressDataPointer+1
  JMP .DecompressMainLoop
.ReturnDecompressToPPU:
  RTS
.CompressedData:
  INY
  BNE .NoPageCross1
  INC <DecompressDataPointer+1
.NoPageCross1:
  LDA [DecompressDataPointer],Y
  BEQ .ReturnDecompressToPPU
  TAX
  INY 
  BNE .NoPageCross2
  INC <DecompressDataPointer+1
.NoPageCross2:
  LDA [DecompressDataPointer],Y
.ToPPULoop:
  STA $2007
  DEX
  BNE .ToPPULoop
  BEQ .DecompressMainLoopReEnter

UploadPalette:
  LDA PPUStatus
  LDA #$3F
  STA PPUAddr
  LDA #$00
  STA PPUAddr
  LDA PalettePointersLow,X
  STA DecompressDataPointer
  LDA PalettePointersHigh,X
  STA DecompressDataPointer+1
  LDY #$1F
.LoopUpload:
  LDA [DecompressDataPointer],Y
  STA $2007
  DEY
  BPL .LoopUpload
  RTS

WaitForNMI:
  LDA <Frame
.WaitNMI:
  CMP <Frame
  BEQ .WaitNMI
  RTS

BankswitchControl:
  LDY #$80
  STY <ZeroWrite
  BNE MainBankSwitch
BankswitchCharacterLower:
  LDY #$A0
  STY <ZeroWrite
  BNE MainBankSwitch
BankswitchCharacterUpper:
  LDY #$C0
  STY <ZeroWrite
  BNE MainBankSwitch
BankswitchProgram:
  LDY #$E0
  STY <ZeroWrite ;Store to get ready to store the last write to $E000
  ORA #$10 ;Disable WRAM if possible.
MainBankSwitch:
  AND #$1F
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA [Zero],Y
  RTS

ReadController:
  LDA ControllerData
  STA ControllerDataOld ;Store old key presses.
  LDX #$01
  STX ControllerOutput
  STX <ControllerData
  DEX
  STX ControllerOutput
.ReadControllers
  LDA $4016
  LSR A
  ROL <ControllerData
  BCC .ReadControllers
  LDA ControllerDataOld
  EOR #$FF ;Reverse keys not pressed last time.
  AND ControllerData ;Test with new keys.
  STA ControllerDataNewlyPressed ;Store buttons that were just pressed.
  RTS


RAMProgram:
  LDA <MapperSetupRAM ;2
  STA $8000 ;3 5
  LSR A ;1 6
  STA $8000 ;3 9
  LSR A ;1 10
  STA $8000 ;3 13
  LSR A ;1 14
  STA $8000 ;3 17
  LSR A ;1 18
  STA $8000 ;3 21
  LDA <GameBankRAM ;2 23
  STA $8000 ;3 26
  LSR A ;1 27
  STA $8000 ;3 30
  LSR A ;1 31
  STA $8000 ;3 34
  LSR A ;1 35
  STA $8000 ;3 38
  LSR A ;1 39
  STA $E000 ;3 42
  JMP [$FFFC] ;3