  .bank 31
  .org $E000

MainScreenCompressed:
  .incbin "Screens/RLECompressedMenuScreen.bin"
GraphicsCompressed:
  .incbin "Graphics/RLECompressedGraphics.chr"

MainPalette:
  .incbin "Graphics/MainPalette.bin"

RleCompressPointersHigh:
  .db HIGH(MainScreenCompressed)
  .db HIGH(GraphicsCompressed)
RleCompressPointersLow:
  .db LOW(MainScreenCompressed)
  .db LOW(GraphicsCompressed)

PalettePointersHigh:
  .db HIGH(MainPalette)
PalettePointersLow:
  .db LOW(MainPalette)


  .include "Program/Subroutines.asm"

Reset:

  .include "Program/Startup.asm"

;Mapper Setup routines.
  LDA #$80
  STA $8000
  LDA #%00001110
  JSR BankswitchControl ;Set to 16KB to swap at 8000-BFFF, 8KB CHR swap, vertical mirroring for SMB.
  LDA #%00000000 ;Doesn't matter. Any number between 00-7F works.
  JSR BankswitchCharacterUpper ;Set character to nothing although it shouldn't do anything.
  JSR BankswitchCharacterLower ;Set character to not garbage.
  LDA #%00011110 ;Low bit doesn't matter since we're in 32KB banking mode.


  LDA #$80
  STA PPUCtrl
  JSR WaitForNMI
  LDA #$00
  STA PPUMask
  LDA #$00
  BIT PPUStatus
  LDA #$00
  STA PPUAddr
  STA PPUAddr
  LDX #$01
  JSR DecompressToPPU
  BIT PPUStatus
  LDA #$20
  STA PPUAddr
  LDA #$00
  STA PPUAddr
  LDX #$00
  JSR DecompressToPPU
  LDX #$00
  JSR UploadPalette
  JSR WaitForNMI
  LDA #$80
  STA PPUCtrl
  LDA #%00011110
  STA PPUMask
  LDA #$00
  STA PPUScroll
  STA PPUScroll
  LDA #$00
  STA PPUOamAddr
  LDA #$02
  STA PPUDma
  LDA #$08
  STA $203 ;X location for cursor.
  LDA #$04
  STA $201 ;Tile
  LDA #$00
  STA $202 ;Attributes
.SelectMenuEngine:
  JSR WaitForNMI
  LDA #$00
  STA PPUOamAddr
  LDA #$02
  STA PPUDma
  JSR ReadController

  LDA <ControllerDataNewlyPressed ;Get button presses.
  AND #UpButton
  BEQ .CheckDown ;No up pressed.
  LDA <ControllerDataNewlyPressed
  AND #DownButton
  BNE .NoCursorMove ;Up and down pressed if true.
  DEC <Cursor ;Not true, move cursor up 1.
  BNE .NoCursorMove ;Escape next.
.CheckDown:
  LDA <ControllerDataNewlyPressed
  AND #DownButton
  BEQ .NoCursorMove ;Not pressed.
  INC Cursor ;Move down.
.NoCursorMove:

;Now range check it.
  LDA Cursor
  BMI .BottomList ;If under 0, wrap around.
  CMP #NumberOfGames-1
  BEQ .AfterRangeCheck ;If last option, skip.
  BCC .AfterRangeCheck ;Or less.
  LDA #$00
  BEQ .BottomList+2 ;A=0, store.
.BottomList
  LDA #NumberOfGames-1
  STA Cursor ;Loop to the last entry on the list.
.AfterRangeCheck:
  LDA <ControllerDataNewlyPressed
  AND #StartButton
  BEQ .NoSelect ;No game selected.
  LDX Cursor
  JMP GameSelected ;Start the data transfer for the character and then bankswitch from RAM after that.
.NoSelect:
  LDA Cursor
  ASL A
  ASL A
  ASL A
  CLC
  ADC #$1F
  STA $200 ;Put cursor X location into RAM for next frame.
  JMP .SelectMenuEngine


GameSelected:
  STX <GameSelectedRAM
  JSR WaitForNMI
  LDA #$00
  STA PPUMask
  STA PPUCtrl ;Turn off screen and NMI's.
  BIT PPUStatus ;Get PPU ready for data.
  LDA #$00
  STA PPUAddr
  STA PPUAddr ;Point to start of graphics.
  LDA GraphicDataBank,X ;Get graphics bank.
  LSR A ;Switch 16KB page value to A with the 8000/A000 switch in the carry.
  PHP ;Save carry
  JSR BankswitchProgram ;Switch program
  PLP ;Get carry
  LDA #$00
  ROR A ;80
  LSR A ;40
  LSR A ;20
  ADC #$80 ;Add 80 to get it in 8000+ range.
  STA <ZeroWrite ;A zero page register for transferring data in aligned chunks.
  LDX #$20 ;Loop times.
  LDY #$00 ;Loop start value.
.WriteGraphicsLoop:
  LDA [Zero],Y ;Get data.
  STA PPUData ;Store data.
  INY ;Next data.
  BNE .WriteGraphicsLoop ;ALL THE DATA!
  INC <ZeroWrite ;Incrememnt data.
  DEX ;Loop more data?
  BNE .WriteGraphicsLoop ;LOOP THE DATA!
  LDX <GameSelectedRAM
  LDA ProgramDataBank,X
  STA <GameBankRAM ;Store the bank for the program to switch later on in the RAM program.
  LDA GameSizes,X ;Get 16 or 32KB switch.
  ASL A
  ASL A
  ASL A
  ADC GameMirroring,X ;Add the bottom bits of the mirroring for the game.
  STA <MapperSetupRAM ;Store for the RAM switch program too.
  LDX #$2D
.CodeToRAMLoop:
  LDA RAMProgram,X
  STA $400,X
  DEX
  BPL .CodeToRAMLoop ;Put all the RAM program into RAM.
  JMP $0400 ;Jump to RAM program to switch the program and start the game.

NMI:
  INC <Frame
IRQ:
  RTI


  .include "Program/GameChangeFile.asm"

  .org $FFFA
  .dw NMI
  .dw Reset
  .dw IRQ