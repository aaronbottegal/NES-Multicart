  .bank 0
;Pgm:
;  .incbin "BinaryData/Program.bin"
;Chr:
;  .incbin "BinaryData/Character.bin"

ZoomingSecretaryPgm:
  .incbin "BinaryData/ZoomingSecretaryProgram.bin" ;32KB

VirusCleanerPgm:
  .incbin "BinaryData/VirusCleanerProgram.bin" ;32KB

AlterEgoPgm:
  .incbin "BinaryData/AlterEgoProgram.bin" ;32KB
AlterEgoChr:
  .incbin "BinaryData/AlterEgoCharacter.bin" ;8KB

BombSweeperChr:
  .incbin "BinaryData/BombSweeperCharacter.bin" ;8KB
BombSweeperPgm:
  .incbin "BinaryData/BombSweeperProgram.bin" ;16KB

ConcentrationRoomPgm:
  .incbin "BinaryData/ConcentrationRoomProgram.bin" ;16KB
ConcentrationRoomChr:
  .incbin "BinaryData/ConcentrationRoomCharacter.bin" ;8KB

LawnMowerChr:
  .incbin "BinaryData/LawnMowerCharacter.bin" ;8KB
LawnMowerPgm:
  .incbin "BinaryData/LawnMowerProgram.bin" ;16KB

ThwaitePgm:
  .incbin "BinaryData/ThwaiteProgram.bin" ;16KB
ThwaiteChr:
  .incbin "BinaryData/ThwaiteCharacter.bin" ;8KB

VirusCleanerChr:
  .incbin "BinaryData/VirusCleanerCharacter.bin" ;8KB

ZoomingSecretaryChr:
  .incbin "BinaryData/ZoomingSecretaryCharacter.bin" ;8KB